log('load script - options.js');
// require
define('app/optionsapp', ['utils/constants', 'utils/storage'], function(CONSTANTS, STORAGE) {

    var optionsData = {
        updateInterval: 30,
        notificationTypeByEnvId: {},
        autoScripts: ""
    };

    var initializeUI = function() {
        log('UI init');
        loadOptionsData().then(function() {
            // get ready DOM
            var notificationTypeHtml = "";
            for (var i=0; i<CONSTANTS.envIds.length; i++) {
                notificationTypeHtml += formatNotificationTypeHtml(CONSTANTS.envIds[i]);
            }
            document.querySelector("#notification-type-environments").innerHTML = notificationTypeHtml;
        
            // elements
            var intervalInputElement = document.querySelector("#update-interval input");
            // initialize UI by options data & hookup events
            // -- update interval
            intervalInputElement.value = optionsData.updateInterval;
            intervalInputElement.addEventListener('change', onUpdateIntervalChanged, false);
            intervalInputElement.addEventListener('keypress', onUpdateIntervalChanged, false);
            intervalInputElement.addEventListener('input', onUpdateIntervalChanged, false); 

            // -- notification type
            CONSTANTS.envIds.forEach(function(envId) {
                var targetValue = optionsData.notificationTypeByEnvId[envId];
                var selectElement = document.querySelector("#notification-type-" + envId + " select");
                var targetIndex = 0;
                var options = selectElement.options;
                for (var i=0; i<options.length; i++) {
                    if (options[i].value === targetValue) {
                        targetIndex = i;
                        break;
                    }
                }
                selectElement.selectedIndex = targetIndex;

                // attach event
                selectElement.addEventListener('change', onNotificationTypeChanged.bind(selectElement, envId), false);
            });

            // -- autoscripts
            var autoScriptsTextareaElement = document.querySelector("#auto-scripts textarea")
            autoScriptsTextareaElement.value = optionsData.autoScripts;
            autoScriptsTextareaElement.addEventListener('change', onAutoScriptsChanged, false);
        });
    };

    var formatNotificationTypeHtml = function(envId) {
        return '<div id="notification-type-' + envId + '" class="environment">'
             +   '<span>' + envId + '</span>'
             +   '<select>' 
             +     getNotificationTypeSelectOptionsHtml()
             +   '</select>'
             + '</div>'
    };

    var getNotificationTypeSelectOptionsHtml = function() {
        var keys = Object.keys(CONSTANTS.notificationTypeEnum);
        var result = "";
        for (var i=0; i<keys.length; i++) {
            var key = keys[i];
            result += '<option value="' + CONSTANTS.notificationTypeEnum[key] + '">' + key + '</option>';
        }

        return result;
    };

    var onUpdateIntervalChanged = function() {
        var interval = parseInt(this.value);
        if (isNaN(interval) || interval == optionsData.updateInterval) {
            return;
        }
        if (interval < 1) {
            // can't be less than 1
            this.value = 1;
            interval = 1;
        }
        log("setting update interval to: " + interval);
        optionsData.updateInterval = interval;
        saveOptionsData();
    };

    var onNotificationTypeChanged = function(envId) {
        var newType = this.options[this.selectedIndex].value;
        log("notification type of " + envId + " is changed to " + newType);
        optionsData.notificationTypeByEnvId[envId] = newType;
        saveOptionsData();
    };

    var onAutoScriptsChanged = function() {
        var scripts = this.value;
        log("update AutoScript");
        optionsData.autoScripts = scripts;
        saveOptionsData();
    };

    var loadOptionsData = function() {
        log('load data');
        var loading = [];
        // update interval
        loading.push(STORAGE.getUpdateInterval());
        
        // options for each environment
        for (var i=0; i<CONSTANTS.envIds.length; i++) {
            loading.push(STORAGE.getOptionsByEnvId(CONSTANTS.envIds[i]));
        }

        // autoScripts
        loading.push(STORAGE.getAutoScripts());

        return Promise.all(loading).then(function(results) {
            var index = 0;
            optionsData.updateInterval = results[index++];
            
            // notification Type
            for (var i=0; i<CONSTANTS.envIds.length; i++) {
                optionsData.notificationTypeByEnvId[CONSTANTS.envIds[i]] = results[index++].notificationType;
            }

            optionsData.autoScripts = results[index++];
        });
    };

    var saveOptionsData = function() {
        log('save data');
        var saving = [];
        // update interval
        saving.push(STORAGE.setUpdateInterval(optionsData.updateInterval));

        // options for each environment
        for (var i=0; i<CONSTANTS.envIds.length; i++) {
            var envId = CONSTANTS.envIds[i];
            var options = {};
            // notification type
            options.notificationType = optionsData.notificationTypeByEnvId[envId]
            saving.push(STORAGE.setOptionsByEnvId(envId, options));
        }

        // autoScripts
        saving.push(STORAGE.setAutoScripts(optionsData.autoScripts));

        return Promise.all(saving);
    };
    return {
        run: function() {
            if(document.readyState === 'complete') { 
                initializeUI();
            }
            else {
                // attach to document loaded
                document.addEventListener('DOMContentLoaded', initializeUI, false);
            }
        }
    };
});

