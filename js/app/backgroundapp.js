// background app
log('load script - backgroundapp.js');
// require
define('app/backgroundapp', ['utils/constants', 'utils/url', 'utils/storage'], function(CONSTANTS, URL, STORAGE) {

    // util method
    var getFloorplanVersion = function(env) {
        var floorplanVersionUrl = URL.getFloorplanUrl(env) + "/version?timestamp=" + new Date().getTime();
        return URL.sendXHR(floorplanVersionUrl).then(function(text) {
            return text.trim();
        })
    };

    var updateSavedVersion = function(env, version) {
        STORAGE.getStatsByEnvId(env).then(function(stats) {
            stats.version = version;
            STORAGE.setStatsByEnvId(env, stats);
        });
    };

    // return all opened floorplan tabs of given envId, if envId is empty, return any opened floorplan tabs
    var findAllOpenedFloorplanTabs = function(env) {
        return new Promise(function(resolve, reject) {
            // searching in current window
            chrome.windows.getCurrent({ populate: true }, function(window) {
                var floorplanTabs = [];
                window.tabs.forEach(function(tab) {
                    var tabEnvId = URL.getFloorplanEnvIdByUrl(tab.url);
                    if ((!env && tabEnvId) || (env && env == tabEnvId)) {
                        floorplanTabs.push(tab);
                    }
                });
                
                resolve(floorplanTabs);
            });
        });
    };

    // show notification
    var onNewVersionArrive = function(env, newVersion) {
        var msg = env + " has a new version";
        log(msg + ": " + newVersion);
        
        // check the settings and do necessary notification
        STORAGE.getOptionsByEnvId(env).then(function(options) {
            switch (options.notificationType) {
                case CONSTANTS.notificationTypeEnum.none:
                    break;
                case CONSTANTS.notificationTypeEnum.desktop:
                    // show notification
                    var notification = new Notification(msg, { icon: 'res/floorplan_icon.png', body: newVersion })
                    notification.onclick = function() {
                        // try to reload current floorplan page
                        findAllOpenedFloorplanTabs(env).then(function(tabs) {
                            if (0 === tabs.length) {
                                // open in new tab if no existing tab
                                var floorplanUrl = URL.getFloorplanUrl(env);
                                chrome.tabs.create({url: floorplanUrl});
                                return;
                            }

                            // if active tab is inside - reload it; otherwise - reload and highlight the first tab
                            var targetTab = tabs[0];
                            for (var i=0; i<tabs.length; i++) {
                                var tab = tabs[i];
                                if (tab.active) {
                                    targetTab = tab;
                                    break;
                                }
                            }
                            var targetTabId = targetTab.id;
                            chrome.tabs.update(targetTabId, { highlighted: true });
                            chrome.tabs.reload(targetTabId, { bypassCache: true });
                        });
                    };
                    break;
                default:
                    log("unkown notification type: " + options.notificationType + " for " + env);
                    break;
            }
        });
    };


    var refreshStats = function() {
        log("refresh stats");
        
        // check if there is any version changed
        CONSTANTS.envIds.forEach(function(env) {
            Promise.all([getFloorplanVersion(env), STORAGE.getStatsByEnvId(env)]).then(function(args) {
                var version = args[0];
                var stats = args[1];
                if (version !== stats.version) {
                    onNewVersionArrive(env, version);
                    updateSavedVersion(env, version);
                }
            });
        });
    };

    var onAlarm = function(alarm) {
        if (alarm && alarm.name != 'refresh')
            return;

        // do refresh
        refreshStats();

        // set next tick
        STORAGE.getUpdateInterval().then(function(interval) {
            log("refresh stats " + interval + " minutes later");
            chrome.alarms.create('refresh', { delayInMinutes: interval }); 
        });
    };

    return {
        run: function() {
            // init
            // log
            chrome.runtime.onStartup.addListener(function() {
                log('runtime startup');
            });
            chrome.runtime.onInstalled.addListener(function() {
                log('runtime installed');
            });
            chrome.runtime.onSuspend.addListener(function() {
                log('runtime suspend');
            });
            chrome.runtime.onSuspendCanceled.addListener(function() {
                log('runtime suspendCanceled');
            });

            // set up period refresh listener
            chrome.alarms.onAlarm.addListener(onAlarm);
            // start period refresh
            onAlarm({name: 'refresh'});

            // browser action TODO: make it a page
            chrome.browserAction.onClicked.addListener(function(tab) {
                log('browserAction clicked, on site: ' + tab.url);
                var env = URL.getFloorplanEnvIdByUrl(tab.url);
                if (!env) {
                    // try to active first floorplan tab
                    findAllOpenedFloorplanTabs().then(function(tabs) {
                        if (tabs.length > 0) {
                            chrome.tabs.update(tabs[0].id, { active: true });
                        }
                    });
                    return;
                }

                // display the version number if it's on floorplan page
                STORAGE.getStatsByEnvId(env).then(function(stats) {
                    var msg = "Floorplan version: " + stats.version;
                    alert(msg);
                });
            });

            // run customer scripts
            chrome.tabs.onUpdated.addListener(function(tabId, changeInfo, tab) {
                // only cares about loading complete status notification
                if (!changeInfo.status || changeInfo.status !== 'complete') {
                    return;
                }
                // check if it's floorplan tab
                if (!URL.isFloorplanUrl(tab.url)) {
                    return;
                }
                // execute script
                log('execute custom script on floorplan page');
                STORAGE.getAutoScripts().then(function (scripts) {
                    if (!scripts) {
                        return;
                    }

                    // remove \n
                    scripts = scripts.replace(/\n/g, "");

                    var wrapper = 'var scriptHost = document.createElement("script");' +
                                    'scriptHost.innerHTML = \'' + scripts + '\';' +
                                    'document.body.appendChild(scriptHost);'; 
                    chrome.tabs.executeScript(tabId, { code: wrapper, runAt: 'document_end' });
                })
            });
        }
    };

});

