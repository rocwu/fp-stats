// storage
log('load script - utils/storage.js');

define('utils/storage', [], function() {
    var storageKey = {
        prefix: 'options-',
        envPrefix: 'env-',
        updateInterval: 'interval',
        autoScripts: 'autoScripts',

        // data
        statsPrefix: 'stats-',
    };
    var defaultSettings = {
        updateInterval: 5,
        notificationType: 'desktop',
        autoScripts: ""
    };

    var save = function(key, data) {
        return new Promise(function(resolve, reject) {
            var keyValuePair = {};
            keyValuePair[key] = data;
            chrome.storage.sync.set(keyValuePair, function() {
                // TODO: check runtime.lastError for error case
                resolve();
            })
        });
    };

    var load = function(key, defaultData) {
        return new Promise(function(resolve, reject) {
            var keyValuePair = {};
            keyValuePair[key] = defaultData;
            chrome.storage.sync.get(keyValuePair, function(result) {
                // TODO: check error
                resolve(result[key]); 
            });
        })
    };

    return {
        // options
        getUpdateInterval: function() {
            var key = storageKey.prefix + storageKey.updateInterval;
            return load(key, defaultSettings.updateInterval);
        },
        setUpdateInterval: function(interval) {
            var key = storageKey.prefix + storageKey.updateInterval;
            return save(key, interval);
        },

        getOptionsByEnvId: function(env) {
            var key = storageKey.prefix + storageKey.envPrefix + env;
            return load(key, {}).then(function (options) {
                // add default values if not set before
                options.notificationType = options.notificationType || defaultSettings.notificationType;
                return options;
            });
        },
        setOptionsByEnvId: function(env, options) {
            var key = storageKey.prefix + storageKey.envPrefix + env;
            return save(key, options);
        },
        getAutoScripts: function() {
            var key = storageKey.prefix + storageKey.autoScripts;
            return load(key, defaultSettings.autoScripts); 
        },
        setAutoScripts: function(scripts) {
            var key = storageKey.prefix + storageKey.autoScripts;
            return save(key, scripts);
        },
        
        // data
        getStatsByEnvId: function(env) {
            return new Promise(function(resolve, reject) {
                var key = storageKey.statsPrefix + env;
                var stats = JSON.parse(localStorage.getItem(key));
                if (!stats) {
                    stats = {
                        version: 'unknown'
                    };
                }
                resolve(stats);
            });
        },
        setStatsByEnvId: function(env, stats) {
            return new Promise(function(resolve, reject) {
                var key = storageKey.statsPrefix + env;
                localStorage.setItem(key, JSON.stringify(stats));
                resolve(stats);
            });
        }
    };
});