// constants
log('load script - utils/constants.js');

define('utils/constants', [], function() {
    return {
        envIds: ['alpha', 'beta', 'prod'],

        notificationTypeEnum: {
            none: 'none',
            desktop: 'desktop'
        }
    };
});