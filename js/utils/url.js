// url
log('load script - utils/url.js');

define('utils/url', ['utils/constants'], function(constants) {
	var hostPrefixByEnvId = {
        alpha: 'alpha',
        beta: 'beta',
        prod: 'www'
    }; 

    // parse url
    var regParseUrl = /^(https?:)\/\/([^\/]+)(?:\/([^\?]+)(?:\?(\S*))?)?/;
    //                   protocol      host      /  path      ? params


    var trimHostPort = function(host) {
        var index = host.indexOf(':');
        if (-1 === index) {
            return host;
        }
        return host.substring(0, index);
    }; 
    var trimPathSlash = function(path) {
        while (path && path.charAt(0) === '/') {
            path = path.substr(1, path.length - 1);
        }
        while (path && path.charAt(path.length - 1) === '/') {
            path = path.substr(0, path.length - 1);
        }
        return path;
    };
    var parseUrl = function(url) {
        var result = regParseUrl.exec(url);
        if (!result) {
            return undefined;
        }
        return {
            protocol: result[1],
            host: trimHostPort(result[2]),
            path: trimPathSlash(result[3]),
            params: result[4]
        }
    };


    // build url


    return {
    	sendXHR: function(url) {
	        return new Promise(function(resolve, reject) {
	            var xhr = new XMLHttpRequest();
	            xhr.onreadystatechange = function(data) {
	                if (xhr.readyState == 4) {
	                    if (xhr.status == 200) {
	                        resolve(xhr.responseText);
	                    } else {
	                        reject(xhr.status);
	                    }
	                }
	            };
	            
	            xhr.open('GET', url, true);
	            xhr.send();
	        });
	    },

        isFloorplanUrl: function(url) {
            var parseResult = parseUrl(url);
            if (!parseResult) {
                return false;
            }

            // check host name
            var hostName = parseResult.host;
            if (hostName !== "localhost" &&
                hostName !== "127.0.0.1" &&
                -1 === hostName.indexOf("homestyler.com")) {
                return false;
            }

            // check path
            var path = parseResult.path;
            return (path === "floorplan" ||
                -1 !== path.indexOf("debug-demo2.html"));
        },

        // TODO: refactor below methods
        getHostName: function(env) {
            return hostPrefixByEnvId[env] + ".homestyler.com";
        },

        getFloorplanUrl: function(env) {
            return "http://" + this.getHostName(env) + "/floorplan";
        },

        getFloorplanEnvIdByUrl: function(url) {
            var reg = /https?:\/\/([a-z\.]+)\.homestyler\.com\/floorplan/
            var result = reg.exec(url);
            if (!result) {
                return undefined;
            }
            
            var targetEnvId = undefined;
            var targetPrefix = result[1];
            for (var i=0; i<constants.envIds.length; i++) {
                var envId = constants.envIds[i];
                var prefix = hostPrefixByEnvId[envId];
                if (prefix === targetPrefix || "www." + prefix === targetPrefix) {
                    targetEnvId = envId;
                    break;
                }
            }
            return targetEnvId;
        }
    };
});
