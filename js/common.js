
// global scope
var log = function(msg) {
    var now = new Date();
    var timestamp = "[" + now.getHours() + ":" + now.getMinutes() + ":" + now.getSeconds() + "." + now.getMilliseconds() + "]";
    console.log(timestamp + msg);
};

// require config
require.config({
    baseUrl: "js"
});