//  background page

require(['common'], function(common) {
    log("load backgroundapp");
    require(['app/backgroundapp'], function(app) {
        app.run();
    });
});